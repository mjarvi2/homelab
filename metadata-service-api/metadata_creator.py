#!/bin/python3

import logging
import subprocess
import json
import ast

def main():
    logging.info("HAJIME!")
    command_runner("ls -la")
    vmids=get_vmids()
    vmids=get_vm_ips(vmids)
    vmids=get_vm_names(vmids)
    vmids=get_vm_tags(vmids)
    vmids=replace_key_with_ip(vmids)
    write_json(vmids)

def sort_dict_by_key(d, reverse = False):
    return dict(sorted(d.items(), reverse = reverse))

def write_json (data):
    with open('metadata.json', 'w') as f:
        json.dump(data,f)

def replace_key_with_ip (vmids):
    new_dict={ }
    for vm in vmids:
        ip=vmids[vm]["ip"]
        vmid=vmids[vm]["vmid"]
        node=vmids[vm]["node"]
        name=vmids[vm]["name"]
        tags=vmids[vm]["tags"]
        new_dict[ip]= { }
        new_dict[ip]["ip"]=ip
        new_dict[ip]["vmid"]=vmid
        new_dict[ip]["node"]=node
        new_dict[ip]["tags"]=tags
    return new_dict

def get_vmids ():
    vm_dict={ }
    file=open("/etc/pve/.vmlist")
    resource_metadata=json.load(file)
    vm_metadata=resource_metadata["ids"]
    for vmid in vm_metadata:
        vm_dict[vmid]= { }
        vm_dict[vmid]["vmid"]=vmid
        vm_dict[vmid]["node"]=vm_metadata[vmid]["node"]
    sorted_dict=sort_dict_by_key(vm_dict)
    logging.debug("sorted_dict %s", sorted_dict)
    return sorted_dict

def get_pvesh_metadata ():
    command='pvesh get /cluster/resources --type vm --output-format json'
    vm_metadata=ast.literal_eval(command_runner(command))
    return vm_metadata

def get_vm_tags (vmids):
    vm_metadata=get_pvesh_metadata()
    for vm in vm_metadata:
        tags_dict=[ ]
        vmid=str(vm["vmid"])
        try:
            tags=vm['tags']
            for tag in tags.split(';'):
                tag_formatted=tag.split("-")
                tags_dict.append({tag_formatted[0]: tag_formatted[1]})
        except:
            tags_dict.append({"unknown": "unknown"})
        vmids[vmid]["tags"]=tags_dict
    return vmids 

def get_vm_names (vmids):
    vm_metadata=get_pvesh_metadata()
    for vm in vm_metadata:
        vmid=str(vm['vmid'])
        vmids[vmid]["name"]=vm["name"]
    return vmids

def get_vm_ip (vmid, node):
    # get a vm ip from a single vm id
    command=f"qm guest cmd {vmid} network-get-interfaces"
    vm_network_interfaces=command_runner(command)
    return vm_network_interfaces

def get_vm_ips (vmids):
    for vm in vmids:
        vm_network_interfaces=get_vm_ip(vm, vmids[vm]["node"])
        ips=[ ]
        ip_to_use="unknown"
        try:
            network_interface_dict=ast.literal_eval(vm_network_interfaces)
            try:
                for interface in network_interface_dict:
                    ips.append(interface["ip-addresses"][0]["ip-address"])
            except:
                ips.append("127.0.0.1")
        except:
            ips.append("127.0.0.1")
        for ip in ips:
            if ip != "127.0.0.1":
                ip_to_use=ip
        try:
            vmids[vm]["ip"]=ip_to_use
        except:
            vmids[vm]["ip"]="unknown"
    return vmids

def command_runner (command):
    command_arr = command.split(" ")
    logging.debug(command_arr)
    result=subprocess.run(command_arr, capture_output=True, text=True).stdout
    return result

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    main()

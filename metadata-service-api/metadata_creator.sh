#!/bin/bash
: ${HOME=/root/metadata-service-api}
: ${LOCK_FILE=/tmp/creator.lock}

if ( set -o noclobber; echo "locked" > "$LOCK_FILE") 2> /dev/null; then
	trap 'rm -f "$LOCK_FILE"; exit $?' INT TERM EXIT
else
	echo "Could not aquire lock file at $LOCK_FILE ... exiting"
	exit
fi

/root/metadata-service-api/metadata_creator.py

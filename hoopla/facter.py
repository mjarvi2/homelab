#!/usr/local/bin/hoopla-venv

# takes tags from db and updates facter on all boxes

import logging
import subprocess
import json
import ast
from vm_utils import command_runner
from tag_vm import grab_node_tags
from syncchecker import grabvmids

def main():
    logging.info("HAJIME!")
    vmidrange=[0, 1000]
    vmids=grabvmids(vmidrange)
    runfacter(vmids)

def runfacter(vmids):
    # UNTESTED. NEEDS WORK.
    for vmid in vmids:
        tagformat=""
        tags=grab_node_tags(vmid)
        logging.info("VMID: %s, Tags: %s", vmid, tags)
        for tag in tags:
            tagformat += tag + "%3E"
        proxmox_node=vm_utils.scrape_vm_proxmox_node(vmid)
        headers = {
            'Authorization': vm_utils.fetch_pve_auth(),
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        url=f"{vm_utils.fetch_api_endpoint()}/api2/json/nodes/{proxmox_node}/qemu/{vmid}/agent/exec"
        logging.info("Request: %s, auth: %s", url, headers)
        response = json.loads(requests.post(url, headers=headers, data={'command': '/bootstrap/facter.sh'}).text)
        logging.info("Response: %s", response)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    main()

#!/usr/local/bin/hoopla-venv

import logging
import subprocess
import json
import ast
import psycopg
import inspect, os.path
import yaml
from inspect import getsourcefile
from os.path import abspath

def main():
    logging.info("HAJIME!")
    grab_config_files()

def grab_config_files():
    filename=abspath(getsourcefile(lambda:0))
    base_path=os.path.dirname(os.path.abspath(filename))
    template_path=f"{base_path}/vm-configs"
    for file in os.listdir(template_path):
        template2db(f"{template_path}/{file}")
    templatepurger(template_path)

def template2db(conf_file):
    logging.info("Found conf file: %s", conf_file)
    with open(conf_file, "r") as stream:
        resources=(yaml.safe_load(stream))
        for key, value in resources.items():
            if value["type"] == "mox-box":
                #mandatory configs first
                moxbox_definition_handler(value)
                moxbox_network_handler(value["vmid"], value["network"])
                #optional tags
                if "tags" in value:
                    moxbox_tags_handler(value["vmid"], value["tags"])

def templatepurger(template_path):
    logging.info("running template purger")
    # find all vmids, if one exists in the db that we dont find, delete it
    dbs=["boxdefinition", "boxtagdefinition", "box_network_definition"]
    vmids=get_vmids_from_files(template_path)
    for db in dbs:
        removaljudge(db, vmids)

def removaljudge(db, vmids):
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = f"SELECT VMID FROM {db}"
            cur.execute(SQL)
            crap = cur.fetchall()
            for vmid in crap:
                if vmid[0] not in vmids:
                    removefromtable(db, vmid[0])

def removefromtable(table, vmid):
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = f"DELETE FROM {table} WHERE vmid = %s"
            DATA = (vmid, )
            try:
                cur.execute(SQL, DATA)
            except:
                cur.execute("ROLLBACK")
                conn.commit()

def get_vmids_from_files(template_path):
    vmids = [ ]
    for file in os.listdir(template_path):
        full_file=f"{template_path}/{file}"
        with open(full_file, "r") as stream:
            resources=(yaml.safe_load(stream))
            for key, value in resources.items():
                if value["type"] == "mox-box":
                    vmids.append(value["vmid"])
    return vmids

def moxbox_definition_handler(values):
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = "SELECT exists (SELECT 1 FROM boxdefinition WHERE vmid = %s) LIMIT 1;"
            DATA = (values["vmid"],)
            cur.execute(SQL, DATA)
            crap = cur.fetchone()
            if not crap[0]:
                SQL = "INSERT INTO boxdefinition(vmid, template, proxmox_host, sockets, cpus, memory, storage, name, status) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s);"
                DATA = (values["vmid"], values["template"], values["node"], values["sockets"], values["cpus"], values["memory"], values["storage"], values["vmname"], "running")
                cur.execute(SQL, DATA)
            else:
                SQL = "UPDATE boxdefinition SET template = %s, proxmox_host = %s, sockets = %s, cpus = %s, memory = %s, storage = %s, name = %s WHERE vmid = %s;"
                DATA = (values["template"], values["node"], values["sockets"], values["cpus"], values["memory"], values["storage"], values["vmname"], values["vmid"])
                cur.execute(SQL, DATA)

def moxbox_tags_handler(vmid, tags):
    logging.debug(tags)
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            for key, value in tags.items():
                logging.debug("key: %s, value: %s", key, value)
                #check if vmid exists
                SQL = f"SELECT exists (SELECT 1 FROM boxtagdefinition WHERE vmid = {vmid}) LIMIT 1;"
                cur.execute(SQL)
                crap = cur.fetchone()
                if not crap[0]:
                    SQL = f"INSERT INTO boxtagdefinition(vmid, {key}) VALUES(%s, %s);"
                    DATA = (vmid, value)
                    try: 
                        cur.execute(SQL, DATA)
                        logging.debug("column does exist")
                    except:
                        cur.execute("ROLLBACK")
                        conn.commit()
                        UPDATE = f"ALTER TABLE boxtagdefinition ADD COLUMN {key} varchar(20);"
                        cur.execute(UPDATE)
                        logging.info("Created new column for %s", key)
                        cur.execute(SQL, DATA)
                else:
                    SQL = f"UPDATE boxtagdefinition SET {key} = %s WHERE vmid = %s;"
                    DATA = (value, vmid)
                    try:
                        cur.execute(SQL, DATA)
                        logging.debug("column does exist")
                    except:
                        cur.execute("ROLLBACK")
                        conn.commit()
                        UPDATE = f"ALTER TABLE boxtagdefinition ADD COLUMN {key} varchar(20);"
                        cur.execute(UPDATE)
                        logging.info("Created new column for %s", key)
                        cur.execute(SQL, DATA)

def moxbox_network_handler(vmid, interfaces):
    logging.info("vmid: %s, values: %s", vmid, interfaces)
    interfacenum=0
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            for interfacename, interfaceconfs in interfaces.items():
                logging.info("interfacename: %s, interfacevals: %s", interfacename, interfaceconfs)
                logging.info("configuring interface: %s for %s", interfacenum, vmid)
                for key, value in interfaceconfs.items():
                    logging.info("key: %s, value %s", key, value)
                    SQL = f"SELECT exists (SELECT 1 FROM box_network_definition WHERE vmid = {vmid}) LIMIT 1;"
                    cur.execute(SQL)
                    crap = cur.fetchone()
                    if not crap[0]:
                        SQL = f"INSERT INTO box_network_definition(vmid, {key}{interfacenum}) VALUES(%s, %s);"
                        DATA = (vmid, value)
                        try:
                            cur.execute(SQL, DATA)
                        except:
                            logging.info("failed")
                            cur.execute("ROLLBACK")
                            conn.commit()
                    else:
                        SQL = f"UPDATE box_network_definition SET {key}{interfacenum} = %s WHERE vmid = %s"
                        DATA = (value, vmid)
                        try:
                            cur.execute(SQL, DATA)
                        except:
                            logging.info("failed")
                            cur.execute("ROLLBACK")
                            conn.commit()
                logging.info("interface num: %s", interfacenum)
                interfacenum+=1
            

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    main()

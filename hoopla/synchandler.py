#!/usr/local/bin/hoopla-venv

import logging
import subprocess
import json
import ast
import psycopg
import inspect, os.path
import yaml
from inspect import getsourcefile
from os.path import abspath
import os
import time
from configure_vm_networking import fix_vm_networking
from vm_utils import grab_node_definition
from tag_vm import vm_tagger

def main():
    logging.info("HAJIME!")
    vmstoreplace = definitionhandler()
    # We don't want to issue a replacement + tagging / networking changes
    tagshandler(vmstoreplace)
    networkhandler(vmstoreplace)

def definitionhandler():
    # if box definition does not match, its should be terminated an a new one created
    vmids=dbchecker("definition")
    replacementforker(vmids)
    return vmids

def tagshandler(exclusion):
    vmids=dbchecker("tags")
    logging.info("Tags Handler vmids: %s", vmids)
    tagsonly=list(set(vmids)-set(exclusion))
    logging.info(tagsonly)
    # do stuff to tag boxes correctly\
    for vmid in tagsonly:
        bouncevmidinsynced(vmid)
        vm_tagger(vmid)

def networkhandler(exclusion):
    vmids=dbchecker("network")
    logging.info("Network Handler vmids: %s", vmids)
    networkonly=list(set(vmids)-set(exclusion))
    logging.info("boxes with networking problems: %s", networkonly)
    # do stuff to give correct network settings
    for vmid in networkonly:
        bouncevmidinsynced(vmid)
        fix_vm_networking(vmid)

def dbchecker(row):
    vmids = [ ]
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = f"SELECT vmid FROM synced WHERE {row} < NOW() - INTERVAL '3 minutes';"
            cur.execute(SQL)
            vms = cur.fetchall()
            for vm in vms:
                vmids.append(vm[0])
    return vmids
    
def killvm(vmid):
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            DATA = (vmid, )
            SQL = f"INSERT INTO boxestokill (vmid) VALUES (%s);"
            cur.execute(SQL, DATA)

def bouncevmidinsynced(vmid):
    logging.info("Vmid: %s", vmid)
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            DATA = (vmid, )
            SQL = f"DELETE FROM synced WHERE vmid = %s;"
            try:
                logging.info("Deleting row")
                cur.execute(SQL, DATA)
            except:
                logging.WARNING("SQL Failure, rolling back")
                cur.execute("ROLLBACK")
                conn.commit()

def launchvm(vmid):
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            DATA = (vmid, )
            SQL = f"INSERT INTO boxestolaunch (vmid) VALUES (%s);"
            cur.execute(SQL, DATA)

def replacementforker(vmids):
    logging.info("vmids: %s", vmids)
    for vmid in vmids:
        logging.info("Killing %s", vmid)
        killvm(vmid)
        bouncevmidinsynced(vmid)
    time.sleep(15)
    for vmid in vmids:
        logging.info("Launching %s", vmid)
        launchvm(vmid)

def command_runner (command):
    command_arr = command.split(" ")
    logging.debug(command_arr)
    result=subprocess.run(command_arr, capture_output=True, text=True).stdout
    return json.loads(result)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    main()

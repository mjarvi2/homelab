import json
import logging
import psycopg
from vm_utils import scrape_vm_proxmox_node
from vm_utils import command_runner
from vm_utils import wait_for_lock
import vm_utils
import requests

def grab_node_networking(vmid, proxmox_node):
    network_dict = { }
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = "SELECT json_object_keys(to_json(json_populate_record(NULL::public.box_network_definition, '{}'::JSON)));"
            cur.execute(SQL)
            columns = cur.fetchall()
            for column in columns:
                stripped_column=column[0]
                if stripped_column != "vmid":
                    logging.info("column: %s", stripped_column)
                    SQL = f"SELECT {stripped_column} FROM box_network_definition WHERE vmid = {vmid};"
                    cur.execute(SQL)
                    value=cur.fetchone()[0]
                    if value is not None:
                        network_dict[stripped_column]=value
                    logging.info("SQL output: %s", value)
            logging.info(network_dict)
            return network_dict

def configure_networking(vmid):
    proxmox_node=scrape_vm_proxmox_node(vmid)
    node_networking=grab_node_networking(vmid, proxmox_node)
    endpoint=vm_utils.fetch_api_endpoint()
    headers = {
        'Authorization': vm_utils.fetch_pve_auth(),
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    url=f"{endpoint}/api2/json/nodes/{proxmox_node}/qemu/{vmid}/config"
    device_counter=0
    for i in range(2):
        wait_for_lock(vmid)
        for key, value in node_networking.items():
            key_counter=f"key{device_counter}"
            key_no_counter=key[:-1]
            if key_no_counter == "net":
                data = {
                    key: f"virtio,bridge={value}"
                }
            if key_no_counter == "ipconfig" and value == "dhcp":
                data = {
                    key: f"ip={value}",
                }
            response = requests.post(
                url,
                data=data,
                headers=headers
            )
            logging.info(response)

def remove_possible_network_interfaces(vmid, proxmox_node):
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = "SELECT json_object_keys(to_json(json_populate_record(NULL::public.box_network_scraped, '{}'::JSON)));"
            cur.execute(SQL)
            columns = cur.fetchall()
            logging.info(columns)
            for column in columns:
                stripped_column=column[0]
                if stripped_column != "vmid":
                    logging.info("column: %s", stripped_column)
                    endpoint=vm_utils.fetch_api_endpoint()
                    headers = {
                        'Authorization': vm_utils.fetch_pve_auth(),
                    }
                    params = {
                        'delete': stripped_column,
                    }
                    url=f"{endpoint}/api2/json/nodes/{proxmox_node}/qemu/{vmid}/config"
                    response = requests.put(url, headers=headers, params=params )
                    logging.info(response)

def fix_vm_networking(vmid):
    wait_for_lock(vmid)
    proxmox_node=scrape_vm_proxmox_node(vmid)
    remove_possible_network_interfaces(vmid, proxmox_node)
    configure_networking(vmid)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    new_vmid=sys.argv[1]
    configure_networking(vmid)

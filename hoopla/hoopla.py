#!/usr/local/bin/hoopla-venv

import logging
import subprocess
import os
import time
import psycopg
from inspect import getsourcefile
from os.path import abspath
import multiprocessing

def main():
    filename=abspath(getsourcefile(lambda:0))
    base_path=os.path.dirname(os.path.abspath(filename))
    logging.info("HAJIME!")
    init()
    start_forker(base_path)   

def init():
    logging.info("Clearing all cache tables")
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = "DELETE FROM boxestokill;"
            cur.execute(SQL)
            SQL = "DELETE FROM boxestolaunch;"
            cur.execute(SQL)
            SQL = "DELETE FROM synced;"
            cur.execute(SQL)
    
def start_forker(base_path):

    #these scripts need to be run rapidly for this whole thing to work
    scripts=[ ['vm_launch_watcher.py', 'vm_kill_watcher.py'], ['template2db.py', 'metadata_creator.py', 'syncchecker.py', 'synchandler.py', 'facter.py'] ]
    functionforker(scripts, base_path)
    logging.info("hit")
    #scripts that need to be constantly run


def scriptrunner(script, base_path):
    logging.info("Running %s", script)
    full_path=f"{base_path}/{script}"
    subprocess.run(full_path)
    os._exit(0)

def functionforker(allscripts, basepath):
    processes=[]
    for scriptarr in allscripts:
        pid = os.fork()
        if pid == 0:
            scriptforker(scriptarr, basepath)
        else:
            processes.append(pid)

    while processes:
        pid, exit_code = os.wait()
        if pid == 0:
            time.sleep(1)
        else:
            logging.info("pid %s, exitcode: %s", pid, exit_code//256)
            processes.remove(pid)

def scriptforker(scripts, base_path):
    processes=[]
    while True:
        for script in scripts:
            pid = os.fork()
            if pid == 0:
                scriptrunner(script, base_path)
            else:
                processes.append(pid)
            time.sleep(1)
 
        while processes:
            pid, exit_code = os.wait()
            if pid == 0:
                time.sleep(1)
            else:
                logging.info("pid %s, exitcode: %s", pid, exit_code//256)
                processes.remove(pid)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    main()

#!/usr/local/bin/hoopla-venv

import logging
import subprocess
import json
import ast
import psycopg
import vm_utils
import requests

def main():
    logging.info("MATRIX...")
    proxmox_nodes=vm_utils.get_nodes()
    vms=get_vm_metadata()
    metadata2db(vms)
    for _ in range(3):
        tags2db(vms)
    networkstripper(vms)
    dbcleaner(vms)


def get_vm_metadata():
    # first grab all proxmox nodes, then query for those
    vm_data = { }
    endpoint=vm_utils.fetch_api_endpoint()
    headers = {
        'Authorization': vm_utils.fetch_pve_auth(),
    }
    url=f"{endpoint}/api2/json/cluster/resources"
    vmids = json.loads(requests.get(url, headers=headers ).text)["data"]
    logging.info("NEW INSTANCE IDS: %s", vmids)
    for vm in vmids:
        if vm["type"]=="qemu":
            vm_data[vm["vmid"]]= { }
            vm_data[vm["vmid"]]["proxmox_node"]=vm["node"]
            vm_data[vm["vmid"]]["status"]=vm["status"]
    for vmid, values in vm_data.items():
        proxmox_node=values["proxmox_node"]
        url=f"{endpoint}/api2/json/nodes/{proxmox_node}/qemu/{vmid}/config"
        try:
            vm_metadata= json.loads(requests.get(url, headers=headers ).text)["data"]
            logging.debug(vm_metadata)
        except:
            vm_metadata=dict({'name': 'unknown', 'sockets': 0, 'cores': 0, 'memory': 0, 'virtio0': 'local-lvm:base-0-disk-0,size=0G'} )
        vm_data[vmid]=vm_metadata
        vm_data[vmid]["proxmox_node"]=proxmox_node
        vm_data[vmid]["status"]=values["status"]
        try:
            vm_data[vmid]["storage"]=vm_metadata["virtio0"].split("size=",1)[1][:-1]
        except:
            vm_data[vmid]["storage"]=0
        if "tags" in vm_metadata:
            logging.debug(vm_metadata["tags"])
            tags=strip_tags(vm_metadata["tags"])
            vm_data[vmid]["tags"]=tags
        logging.debug(vm_metadata)
    return vm_data

def strip_tags(tags):
    tags_arr=[ ]
    for tag in tags.split(';'):
        tag_formatted=tag.split(".")
        tags_arr.append({tag_formatted[0]: tag_formatted[1]})
    return tags_arr

def metadata2db(vm_metadata):
    # push stuff to db
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            for vmid, values in vm_metadata.items():
                SQL = "SELECT exists (SELECT 1 FROM boxscraped WHERE vmid = %s) LIMIT 1;"
                DATA = (vmid,)
                cur.execute(SQL, DATA)
                crap = cur.fetchone()
                if not crap[0]:
                    try:
                        SQL = "INSERT INTO boxscraped(vmid, proxmox_host, name, status, sockets, cpus, memory, storage) VALUES(%s, %s, %s, %s, %s, %s, %s, %s);"
                        DATA = (vmid, values["proxmox_node"], values["name"], values["status"], values["sockets"], values["cores"], values["memory"], values["storage"] )
                        cur.execute(SQL, DATA)
                    except:
                        cur.execute("ROLLBACK")
                        conn.commit()
                else:
                    try:
                        SQL = "UPDATE boxscraped SET name = %s, proxmox_host = %s, status = %s, sockets = %s, cpus = %s, memory = %s, storage = %s WHERE vmid = %s;"
                        DATA = (values["name"], values["proxmox_node"], values["status"], values["sockets"], values["cores"], values["memory"], values["storage"], vmid)
                        cur.execute(SQL, DATA)
                    except:
                        cur.execute("ROLLBACK")
                        conn.commit()

def tags2db(vm_metadata):
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = "DELETE FROM boxtagscraped"
            try:
                cur.execute(SQL)
            except:
                logging.info("SQL Failure")
                cur.execute("ROLLBACK")
                conn.commit()
            for vmid, values in vm_metadata.items():
                SQL = "SELECT exists (SELECT 1 FROM boxtagscraped WHERE vmid = %s) LIMIT 1;"
                DATA = (vmid,)
                cur.execute(SQL, DATA)
                crap = cur.fetchone()
                if not crap[0]:
                    SQL = "INSERT INTO boxtagscraped(vmid) VALUES(%s);"
                    DATA = (vmid,)
                    cur.execute(SQL, DATA)

                if "tags" in values:
                    for tag in values["tags"]:
                        [[k, v]] = tag.items()
                        SQL = f"UPDATE boxtagscraped SET {k} = %s WHERE vmid = %s;"
                        DATA = (v, vmid)
                        if k != "template":
                            try:
                                cur.execute(SQL, DATA)
                            except:
                                cur.execute("ROLLBACK")
                                conn.commit()
                                UPDATE = f"ALTER TABLE boxtagscraped ADD COLUMN {k} varchar(20);"
                                cur.execute(UPDATE)
                            cur.execute(SQL, DATA)
                        else:
                            SQL = f"UPDATE boxscraped SET template = %s WHERE vmid = %s;"
                            DATA = (v, vmid)
                            cur.execute(SQL, DATA)

def dbcleaner(vm_metadata):
    dbs = ["boxscraped", "boxtagscraped", "box_network_scraped"]
    logging.info("cleaning up non-existent resources")
    for db in dbs:
        removaljudge(db, vm_metadata)

def removaljudge(db, vm_metadata):
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = f"SELECT VMID FROM {db}"
            cur.execute(SQL)
            crap = cur.fetchall()
            for vmid in crap:
                if vmid[0] not in vm_metadata.keys():
                    removefromtable(db, vmid[0])

def removefromtable(table, vmid):
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = f"DELETE FROM {table} WHERE vmid = %s"
            DATA = (vmid, )
            try:
                cur.execute(SQL, DATA)
            except:
                cur.execute("ROLLBACK")
                conn.commit()

def networkstripper(vm_metadata):
    for vmid, values in vm_metadata.items():
        interfacenum=0
        for i in range(2):
            key=f"net{interfacenum}"
            if key in values:
                rawvalue=values[key].split("bridge=",1)[1]
                network2db(vmid, key, rawvalue)
            elif key not in values:
                network2db(vmid, key, None)
            key=f"ipconfig{interfacenum}"
            if key in values:
                rawvalue=values[key].split("ip=",1)[1]
                network2db(vmid, key, rawvalue)
            elif key not in values:
                network2db(vmid, key, None)
            interfacenum=+1

def network2db(vmid, key, value):
    logging.info("vmid: %s, tag: %s, value %s", vmid, key, value)
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:            
            SQL = "SELECT exists (SELECT 1 FROM box_network_scraped WHERE vmid = %s) LIMIT 1;"
            DATA = (vmid,)
            cur.execute(SQL, DATA)
            crap = cur.fetchone()
            if not crap[0]:
                logging.info("box not in db yet")
                SQL = f"INSERT INTO box_network_scraped(vmid, {key}) VALUES({vmid}, %s)"
            else:
                SQL = f"UPDATE box_network_scraped SET {key} = %s WHERE vmid = {vmid}"
            DATA = (value, )
            try:
                cur.execute(SQL, DATA)
            except:
                logging.info("SQL Failure")
                cur.execute("ROLLBACK")
                conn.commit()
                
if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    main()

#!/usr/local/bin/hoopla-venv

import logging
import subprocess
import json
import ast
import psycopg
import inspect, os.path
import yaml
from inspect import getsourcefile
import os

def main():
    logging.info("Looking for vms to launch")
    vmids=pollforvmids()
    launchforker(vmids)


def pollforvmids():
    vmids = [ ] 
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = "SELECT vmid FROM boxestolaunch;"
            cur.execute(SQL)
            vms = cur.fetchall()
            for vmid in vms:
                vmids.append(vmid[0])
            SQL = "DELETE FROM boxestolaunch;"
            cur.execute(SQL)
            return vmids

def launchvm(vmid):
    logging.info("Launching %s", vmid)
    command=f"launch-vm {vmid}"
    command_runner(command)
    os._exit(0)

def launchforker(vmids):
    logging.info(vmids)
    for vmid in vmids:
        newpid = os.fork()
        if newpid == 0:
            launchvm(vmid)
        else:
            pids = (os.getpid(), newpid)
            logging.info("parent: %d, child %d" % pids)

def command_runner (command):
    command_arr = command.split(" ")
    logging.debug(command_arr)
    result=subprocess.run(command_arr, capture_output=True, text=True).stdout
    return result

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    main()

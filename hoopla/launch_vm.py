#!/usr/local/bin/hoopla-venv

import logging
import subprocess
import json
import ast
import psycopg
import sys
import time
import os
import vm_utils
from tag_vm import tag_vm
from tag_vm import grab_node_tags
from configure_vm_networking import configure_networking
import requests

def main(new_vmid):
    logging.info("launching box")
    clone_template(new_vmid)
    vm_utils.wait_for_lock(new_vmid)
    configure_vm(new_vmid)
    vm_utils.wait_for_lock(new_vmid)
    vm_utils.configure_storage(new_vmid)
    vm_utils.wait_for_lock(new_vmid)
    configure_networking(new_vmid)
    tag_vm(new_vmid)
    vm_utils.wait_for_lock(new_vmid)
    vm_utils.start_vm(new_vmid)
    vm_utils.wait_for_lock(new_vmid)
    bootstrap_vm(new_vmid)

def find_template_vmid(template_name, proxmox_node):
    logging.info("Template name: %s, proxmox node: %s", template_name, proxmox_node)
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = "SELECT vmid FROM boxscraped WHERE name = %s AND proxmox_host = %s"
            DATA = (template_name, proxmox_node)
            cur.execute(SQL, DATA)
            template_vmid = cur.fetchone()
    return template_vmid[0]

def clone_template(vmid):
    template_name=vm_utils.grab_node_definition(vmid, "template")
    proxmox_node=vm_utils.grab_node_definition(new_vmid, "proxmox_host")
    template_vmid=find_template_vmid(template_name, proxmox_node)
    name=vm_utils.grab_node_definition(vmid, "name")
    logging.info("Found name: %s", name)
    endpoint=vm_utils.fetch_api_endpoint()
    headers = {
        'Authorization': vm_utils.fetch_pve_auth()
    }
    url=f"{endpoint}/api2/json/nodes/{proxmox_node}/qemu/{template_vmid}/clone"
    response = json.loads(requests.post(url, headers=headers, data={'newid': vmid, 'name': name, 'full': 1}).text)
    logging.info("Response: %s", response)

def configure_vm(vmid):
    proxmox_node=vm_utils.scrape_vm_proxmox_node(vmid)
    endpoint=vm_utils.fetch_api_endpoint()
    sockets=vm_utils.grab_node_definition(new_vmid, "sockets")
    cpus=vm_utils.grab_node_definition(new_vmid, "cpus")
    vm_memory=vm_utils.grab_node_definition(new_vmid, "memory")
    headers = {
        'Authorization': vm_utils.fetch_pve_auth()
    }
    url=f"{endpoint}/api2/json/nodes/{proxmox_node}/qemu/{vmid}/configure"
    response = json.loads(requests.post(url, headers=headers, data={'sockets': sockets, 'cores': cpus, 'memory': vm_memory}).text)

def bootstrap_vm(vmid):
    logging.info("Giving the box some time to boot before running bootstrap")
    time.sleep(60)
    proxmox_node=vm_utils.scrape_vm_proxmox_node(vmid)
    vm_tags=grab_node_tags(vmid)
    command="sh%00-c%00%2Fbootstrap%2Fbootstrap.sh%20"#%27"
    #for index, tag in enumerate(vm_tags):
    #    if index != len(vm_tags) - 1:
    #        command = command + tag + "%3E"
    #    else:
    #        command = command + tag + "%27"
    headers = {
        'Authorization': vm_utils.fetch_pve_auth(),
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    url=f"{vm_utils.fetch_api_endpoint()}/api2/json/nodes/{proxmox_node}/qemu/{vmid}/agent/exec"
    logging.info("Request: %s, auth: %s", url, headers)
    response = json.loads(requests.post(url, headers=headers, data={'command': '/bootstrap/bootstrap.sh'}).text)
    logging.info("Response: %s", response)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    new_vmid=sys.argv[1]
    main(new_vmid)

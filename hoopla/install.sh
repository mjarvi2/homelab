#!/bin/bash
dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

python-install () {
	printf "Installing python and venv\n"
	chmod +x $dir/hoopla-init/config/python-setup.sh 
	$dir/hoopla-init/config/python-setup.sh
	chmod +x $dir/hoopla-init/tests/python-test.sh
	$dir/hoopla-init/tests/python-test.sh
}

postgres-install () {
	printf "Injecting creds into ddl\n"
	chmod +x $dir/hoopla-init/config/postgres-creds-injection.py
	$dir/hoopla-init/config/postgres-creds-injection.py $dir
	printf "Installing postgresql\n"
	chmod +x $dir/hoolpa-init/config/postgres-setup.sh
	$dir/hoopla-init/config/postgres-setup.sh
	chmod +x $dir/hoopla-init/tests/postgres-test.py
	$dir/hoopla-init/tests/postgres-test.py
	printf "Initializing metadata\n"
	$dir/metadata_creator.py
}

etc-setup(){
	chmod +x $dir/*
        sudo ln -s $dir/launch_vm.py /usr/local/bin/launch-vm
        sudo ln -s $dir/kill_vm.py /usr/local/bin/kill-vm
        mkdir $dir/logs
	sudo apt-get install postfix -y
	sudo mkfifo /var/spool/postfix/public/pickup
        sudo systemctl restart postfix

}

service-setup(){
	sedsafedir=$(printf '%s\n' "$dir" | sed -e 's/[]\/$*.^[]/\\&/g');
        sed -i "s/SCRIPT_PATH/$sedsafedir/g" $dir/hoopla-init/config/hoopla.service
        sudo cp $dir/hoopla-init/config/hoopla.service /lib/systemd/system/hoopla.service
	sudo systemctl daemon-reload
	sudo systemctl enable hoopla.service
	sudo systemctl start hoopla.service
}

main () {
	printf "HAJIME!\n"
	homedir=$( getent passwd "$USER" | cut -d: -f6 )
	sudo chmod og+X /home $homedir
	etc-setup
	python-install
	postgres-install
	service-setup
	printf "DUNZO!\n"
}
main

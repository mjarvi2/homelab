#!/usr/local/bin/hoopla-venv

import logging
import subprocess
import json
import ast
import psycopg
import sys
import time
import vm_utils
import requests

def tag_vm(vmid):
    vm_utils.wait_for_lock(vmid)
    vm_tagger(vmid)
   
def vm_tagger(vmid):
    tag_arr = [ ]
    proxmox_node=vm_utils.scrape_vm_proxmox_node(vmid)
    template=vm_utils.grab_node_definition(vmid, "template")
    tag_arr.append(f"template.{template}")
    other_tags=grab_node_tags(vmid)
    tag_arr = tag_arr + other_tags
    tags=''
    for tag in tag_arr:
        tags+=tag+','
    endpoint=vm_utils.fetch_api_endpoint()
    headers = {
        'Authorization': vm_utils.fetch_pve_auth(),
    }
    params = {
        'tags': {tags}
    }
    url=f"{endpoint}/api2/json/nodes/{proxmox_node}/qemu/{vmid}/config"
    response = requests.put(url, headers=headers, params=params )
    logging.info(response.text)

def grab_node_tags(vmid):
    tags_arr = [ ]
    logging.info("tags arr: %s", tags_arr)
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            # grab table names
            SQL = "SELECT json_object_keys(to_json(json_populate_record(NULL::public.boxtagdefinition, '{}'::JSON)));"
            cur.execute(SQL)
            columns = cur.fetchall()
            logging.info("Columns: %s", columns)
            for column in columns:
                logging.info("Column: %s", column)
                stripped_column=column[0]
                if stripped_column != "vmid":
                    SQL = f"SELECT {stripped_column} FROM boxtagdefinition WHERE vmid = {vmid};"
                    cur.execute(SQL)
                    try:
                        value=cur.fetchone()[0]
                        logging.info("Stripped column: %s, Value: %s", stripped_column, value)
                        if value is not None:
                            tags_arr.append(stripped_column+"."+value)
                        else:
                            logging.info("Value is none, this is a bug")
                    except:
                        logging.info("probably a box on a dead proxmox host")
    return tags_arr


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    new_vmid=sys.argv[1]
    tag_vm(new_vmid)

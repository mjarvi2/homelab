#!/usr/local/bin/hoopla-venv

import logging
import subprocess
import json
import ast
import psycopg
import inspect, os.path
import yaml
from inspect import getsourcefile
from os.path import abspath
import os
import time
from vm_utils import command_runner
from datetime import datetime, timezone

def main():
    logging.info("HAJIME!")
    vmidrange = grabvmidrange()
    vmids = grabvmids(vmidrange)
    vmidcheck(vmids)
    comparerunner(vmids)

def grabvmidrange():
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            logging.info("Grabbing vmid range")
            SQL = "SELECT lowvmid, highvmid FROM proxmoxcreds;"
            cur.execute(SQL)
            return cur.fetchall()[0]

def grabvmids(vmidrange):
    vmidsinrange = [ ] 
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = "SELECT vmid FROM boxdefinition;"
            cur.execute(SQL)
            vmids_def = cur.fetchall()
            logging.info("vmids_def: %s", vmids_def)
            SQL = "SELECT vmid FROM boxscraped;"
            cur.execute(SQL)
            vmids_scraped = cur.fetchall()
            logging.info("vmids_def: %s", vmids_scraped)
            vmids = vmids_def + vmids_scraped
            for vmid in vmids:
                logging.info(vmid)
                if vmid[0] > vmidrange[0] and vmid[0] < vmidrange[1]:
                    vmidsinrange.append(vmid[0])
    return vmidsinrange

def vmidcheck(vmids):
    addvmids(vmids)
    removevmids(vmids)

def addvmids(vmids):
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            for vmid in vmids:
                logging.info(vmid)
                SQL = "SELECT exists (SELECT 1 FROM synced WHERE vmid = %s) LIMIT 1;"
                DATA = (vmid,)
                cur.execute(SQL, DATA)
                crap = cur.fetchone()
                if not crap[0]:
                    logging.info("box not in db yet")
                    SQL = f"INSERT INTO synced(vmid) VALUES(%s)"
                    DATA = (vmid, )
                    try:
                        cur.execute(SQL, DATA)
                    except:
                        logging.info("SQL Failure")
                        cur.execute("ROLLBACK")
                        conn.commit()

def getvmidsfromsynced():
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = "SELECT vmid FROM synced"
            cur.execute(SQL)
            vmids = cur.fetchall()
            return vmids

def removevmids(vmids):
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            crap = getvmidsfromsynced()
            for vmid in crap:
                if vmid[0] not in vmids:
                    SQL = "DELETE FROM synced WHERE vmid = %s"
                    DATA = (vmid[0],)
                    try:
                        logging.info("Removing vm from synced table")
                        logging.info("SQL: %s", SQL)
                        cur.execute(SQL, DATA)
                    except:
                        logging.info("SQL Failure")
                        cur.execute("ROLLBACK")
                        conn.commit()

def comparetags(vmid):
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = "SELECT json_object_keys(to_json(json_populate_record(NULL::public.boxtagdefinition, '{}'::JSON)));"
            cur.execute(SQL)
            columns = cur.fetchall()
            for column in columns:
                stripped_column=column[0]
                if stripped_column != "vmid":
                    logging.info(stripped_column)
                DATA=(vmid,)
                SQL = f"SELECT {stripped_column} from boxtagscraped WHERE vmid=%s;"
                try:
                    cur.execute(SQL, DATA)
                    valuescraped=cur.fetchone()[0]
                    logging.info("Scraped Value: %s", valuescraped)
                except:
                    logging.info("SQL Failure on compare 1, rolling back")
                    cur.execute("ROLLBACK")
                    conn.commit()
                    valuescraped=None
                SQL = f"SELECT {stripped_column} from boxtagdefinition WHERE vmid=%s;"
                try:
                    cur.execute(SQL, DATA)
                    valuedef=cur.fetchone()[0]
                    logging.info("Defined Value: %s", valuescraped)
                except:
                    logging.info("SQL Failure on compare 2, rolling back")
                    cur.execute("ROLLBACK")
                    conn.commit()
                    valuedef=None
                if valuedef != valuescraped:
                    return False
            return True

def tagschecker(vmids):
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            for vmid in vmids:
                synced=comparetags(vmid)
                timestamp = datetime.now(timezone.utc)
                DATA=(timestamp, vmid)
                if synced:
                    SQL = f"UPDATE synced SET tags = %s WHERE vmid = %s"
                    try:
                        cur.execute(SQL, DATA)
                    except:
                        logging.info("SQL Failure, rolling back")
                        cur.execute("ROLLBACK")
                        conn.commit()

def comparerunner(vmids):
    dbs = { "db1": { "db1": "boxdefinition", "db2": "boxscraped", "row": "definition" },
            "db2": { "db1": "box_network_definition", "db2": "box_network_scraped", "row": "network"} }
    for db in dbs.values():
        logging.info("db: %s", db)
        dbcompare (db["db1"], db["db2"], db["row"])
        tagschecker(vmids)

def dbcompare (db1, db2, row):
    logging.info("comparing %s to %s and inserting into %s", db1, db2, row)
    vmids=getvmidsfromsynced()
    logging.info(vmids)
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            for vmid in vmids:
                SQL = f"SELECT * FROM {db1} WHERE vmid = %s"
                DATA = (vmid[0], )
                cur.execute(SQL, DATA)
                crap1 = cur.fetchone()
                logging.info(crap1)
                SQL = f"SELECT * FROM {db2} WHERE vmid = %s"
                DATA = (vmid[0], )
                cur.execute(SQL, DATA)
                crap2 = cur.fetchone()
                logging.info(crap2)
                if crap1 == crap2:
                    timestamp = datetime.now(timezone.utc)
                    DATA = (timestamp, vmid[0])
                    logging.info("Resources are in sync for %s", vmid)
                    SQL = f"UPDATE synced SET {row} = %s WHERE vmid = %s"
                    try:
                        logging.info("Adding timestamp to table")
                        cur.execute(SQL, DATA)
                    except:
                        logging.info("SQL Failure")
                        cur.execute("ROLLBACK")
                        conn.commit()

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    main()

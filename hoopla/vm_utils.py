# Various functions needed by different scripts
import psycopg
import time
import logging
import subprocess
import json
import requests
import urllib3
import re

def wait_for_lock(vmid):
    proxmox_node=scrape_vm_proxmox_node(vmid)
    endpoint=fetch_api_endpoint()
    headers = {
        'Authorization': fetch_pve_auth()
    }
    locked="lock"
    while locked == "lock":
        url=f"{endpoint}/api2/json/nodes/{proxmox_node}/qemu/{vmid}/status/current"
        response=json.loads(requests.get(url, headers=headers).text)
        if response["data"] is not None:
            if "lock" in response["data"]:
                logging.info("waiting for lock")
                time.sleep(3)
            else:
                logging.info("no lock")
                locked="unlocked"
        else:
            logging.info("Assuming vmid is not in use")
            locked="unlocked"

def grab_node_definition(vmid, column):
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = f"SELECT {column} FROM boxdefinition WHERE vmid = {vmid};"
            cur.execute(SQL)
            return cur.fetchone()[0]

def start_vm(vmid):
    proxmox_node=scrape_vm_proxmox_node(vmid)
    endpoint=fetch_api_endpoint()
    headers = {
        'Authorization': fetch_pve_auth()
    }
    url=f"{endpoint}/api2/json/nodes/{proxmox_node}/qemu/{vmid}/status/start"
    response = json.loads(requests.post(url, headers=headers).text)
    logging.info("Response: %s", response)
    return response

def configure_storage(vmid):
    endpoint=fetch_api_endpoint()
    proxmox_node=scrape_vm_proxmox_node(vmid)
    headers = {
        'Authorization': fetch_pve_auth()
    }
    # regex to get the current disk size
    search=re.search(r'size=(\d+)G', get_vm_config(vmid)["virtio0"])
    disk_current=int(search.group(1))
    # grab desired
    disk_desired=int(grab_node_definition(vmid, "storage"))
    print("disk_current: %s, disk_desired: %s", disk_current, disk_desired)
    if disk_current < disk_desired:
        url=f"{endpoint}/api2/json/nodes/{proxmox_node}/qemu/{vmid}/resize?disk=virtio0&size={disk_desired}G"
        print(url)
        response = json.loads(requests.put(url, headers=headers).text)
        print(response)
        

def get_vm_config(vmid):
    proxmox_node=scrape_vm_proxmox_node(vmid)
    endpoint=fetch_api_endpoint()
    headers = {
        'Authorization': fetch_pve_auth()
    }
    disk=int(grab_node_definition(vmid, "storage"))
    url=f"{endpoint}/api2/json/nodes/{proxmox_node}/qemu/{vmid}/config"
    response=json.loads(requests.get(url, headers=headers).text)["data"]
    return response

def get_nodes():
    # get proxmox nodes with their statuses
    proxmox_nodes = { }
    endpoint=fetch_api_endpoint()
    headers = {
        'Authorization': fetch_pve_auth()
    }
    url=f"{endpoint}/api2/json/nodes"
    response=json.loads(requests.get(url, headers=headers).text)["data"]
    for box in response:
        proxmox_nodes[box["node"]]=box["status"]
    return proxmox_nodes

def scrape_vm_proxmox_node(vmid):
    logging.info("Looking for proxmox node for vmid: %s", vmid)
    headers = {
        'Authorization': fetch_pve_auth(),
    }
    url=f"{fetch_api_endpoint()}/api2/json/cluster/resources"
    response = requests.get(url, headers=headers).json()["data"]
    for resource in response:
        for key in resource:
            if key == "vmid" and int(resource[key])==int(vmid):
                logging.info("Found node %s for vmid %s", resource["node"], vmid) 
                return resource["node"]

def fetch_pve_auth():
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = f"SELECT * FROM proxmoxcreds"
            cur.execute(SQL)
            crap=cur.fetchone()
            auth=f"PVEAPIToken={crap[0]}={crap[1]}"
            return auth

def fetch_api_endpoint():
    with psycopg.connect("postgresql://hoopla:hoopla@localhost:5432") as conn:
        with conn.cursor() as cur:
            SQL = f"SELECT * FROM proxmoxcreds"
            cur.execute(SQL)
            return cur.fetchone()[2]

def command_runner (command):
    command_arr = command.split(" ")
    logging.debug(command_arr)
    result=subprocess.run(command_arr, capture_output=True, text=True).stdout
    crap=json.loads(result)
    if list(crap.keys())[0] != "data":
        logging.warn("Box failed to launch")
    return json.loads(result)

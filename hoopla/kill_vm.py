#!/usr/local/bin/hoopla-venv
import logging
import subprocess
import sys
import psycopg
import json
import vm_utils
import requests

def main(vmid):
    logging.info("killing box %s", vmid)
    stop_vm(vmid)
    vm_utils.wait_for_lock(vmid)
    kill_vm(vmid)

def kill_vm(vmid):
    proxmox_node=vm_utils.scrape_vm_proxmox_node(vmid)
    endpoint=vm_utils.fetch_api_endpoint()
    headers = {
        'Authorization': vm_utils.fetch_pve_auth(),
    }
    params = {
        'purge': '1',
        'destroy-unreferenced-disks': '1',
    }
    url=f"{endpoint}/api2/json/nodes/{proxmox_node}/qemu/{vmid}"
    response = requests.delete(url, headers=headers, params=params )
    logging.info(response)

def stop_vm(vmid):
    proxmox_node=vm_utils.scrape_vm_proxmox_node(vmid)
    endpoint=vm_utils.fetch_api_endpoint()
    headers = {
        'Authorization': vm_utils.fetch_pve_auth(),
    }
    url=f"{endpoint}/api2/json/nodes/{proxmox_node}/qemu/{vmid}/status/stop"
    response = requests.post(url, headers=headers )
    logging.info(response)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    new_vmid=sys.argv[1]
    main(new_vmid)

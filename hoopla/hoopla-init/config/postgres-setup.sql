--set up user hoopla
CREATE USER hoopla WITH PASSWORD 'hoopla';
GRANT postgres TO hoopla;

--Create tables
CREATE TABLE proxmoxcreds(
	token_id	varchar(36)	PRIMARY KEY,
	token_secret	TEXT		NOT NULL,
	proxmox_ip	TEXT		NOT NULL,
	lowvmid		smallint,
	highvmid	smallint	
);

--box data that is defined
CREATE TABLE boxdefinition(
	vmid		smallint	PRIMARY KEY,
	name		varchar(20),
	template	varchar(20),
	sockets		smallint,
	cpus		smallint,
	memory		smallint,
	storage		smallint,
	status		varchar(10),
	proxmox_host	varchar(20)
);

--box data that is scraped
CREATE TABLE boxscraped(
        vmid            smallint	PRIMARY KEY,
        name            varchar(20),
        template        varchar(20),
	sockets		smallint,
	cpus		smallint,
	memory		smallint,
	storage		smallint,
	status		varchar(10),
	proxmox_host	varchar(10)
);

-- table for tags, scripts will add columns to this table
CREATE TABLE boxtagdefinition(
        vmid            smallint,
	FOREIGN KEY (vmid) REFERENCES boxdefinition(vmid)
);

-- same as above, but scraped
CREATE TABLE boxtagscraped(
        vmid            smallint	PRIMARY KEY
);

-- defined network
CREATE TABLE box_network_definition(
        vmid            smallint        PRIMARY KEY,
        net0         	varchar(20),
	ipconfig0       varchar(20),
        net1         	varchar(20),
	ipconfig1       varchar(20)
);

-- scraped networking resources
CREATE TABLE box_network_scraped(
        vmid            smallint        PRIMARY KEY,
        net0            varchar(20),
        ipconfig0       varchar(20),
        net1            varchar(20),
        ipconfig1       varchar(20)
);

-- are synced vs defined resources in sync?
CREATE TABLE synced(
	vmid		smallint	PRIMARY KEY,
	definition	TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	tags		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	network		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- table that will be read to launch new boxes
CREATE TABLE boxestolaunch(
	vmid		smallint	PRIMARY KEY
);

-- table that will be read to kill boxes
CREATE TABLE boxestokill(
	vmid		smallint	PRIMARY KEY
);

INSERT INTO proxmoxcreds(token_id, token_secret, proxmox_ip, lowvmid, highvmid)
VALUES ('TOKENID', 'TOKENSECRET', 'PROXMOXIP', LOWVMID, HIGHVMID);

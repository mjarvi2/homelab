#!/bin/bash

prereqs () {
	printf "installing postgresql\n"
	PKG_OK=$(dpkg-query -W --showformat='${Status}\n' 'postgresql'|grep "install ok installed")
	if [ "" = "$PKG_OK" ]; then
		sudo apt install ca-certificates -y
		wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
		sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
		sudo apt update
		sudo apt install postgresql -y
	fi
}

init-db () {
	dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
	printf "initializing the db\n"
	DB_NAME=hoopla
	sudo -u postgres psql -tc "DROP DATABASE IF EXISTS $DB_NAME;"
	sudo -u postgres psql -tc "CREATE DATABASE hoopla;"
	sudo -u postgres psql -f $dir/postgres-setup.sql $DB_NAME
}

main () {
	prereqs
	init-db 
}
main

#!/bin/bash

venv-checker () {
	venv_name="$HOME/hoopla-venv"
	printf "checking if $venv_name exists\n"
	if [ -d "$venv_name" ];
		then
			printf "venv already exists\n"
		else
			printf "venv does not exist, making a new one\n"
			venv-configurator $venv_name
	fi
}

venv-configurator () {
	venv_name=$1
	printf "Installing python 3.10 venv\n"
	sudo apt install python3.10-venv python3-pip -y
	printf "psycopg doesnt work well in venvs\n"
	pip install psycopg
	printf "creating the venv\n"
	python3 -m venv "$venv_name"
	printf "configuring pip in the venv\n"
	$venv_name/bin/pip3 install -r hoopla-init/config/requirements.txt
	sudo ln -s $venv_name/bin/python /usr/local/bin/hoopla-venv
}

main () {
	venv-checker
}

main

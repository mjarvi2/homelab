#!/usr/local/bin/hoopla-venv
import logging
import datetime
import subprocess
import yaml
import sys
import fileinput

def main (base_dir):
    logging.info("Injecting creds into the ddl")
    conf_file=f"{base_dir}/hoopla.conf"
    ddl_file=f"{base_dir}/hoopla-init/config/postgres-setup.sql"
    with open(conf_file, "r") as stream:
        confs=(yaml.safe_load(stream))
    logging.info("Found configurations: %s", confs)
    replace(ddl_file, "TOKENID", confs["api-endpoint"]["token-id"])
    replace(ddl_file, "TOKENSECRET", confs["api-endpoint"]["token-secret"])
    replace(ddl_file, "PROXMOXIP", confs["api-endpoint"]["ip"])
    replace(ddl_file, "LOWVMID", str(confs["vmid-range"]["start"]))
    replace(ddl_file, "HIGHVMID", str(confs["vmid-range"]["end"]))

def replace (ddl_file, old, new):
    with fileinput.FileInput(ddl_file, inplace=True, backup='.bak') as file:
        for line in file:
            print(line.replace(old, new), end='')

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    main(sys.argv[1])

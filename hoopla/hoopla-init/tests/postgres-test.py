#!/usr/local/bin/hoopla-venv
import logging
import datetime
import subprocess
import json

def main ():
    if api_check():
        logging.info("POSTGRES/API TEST RESULT: PASS\n")
    else:
        loggiung.info("POSTGRES/API TEST RESULT: FAIL\n")

def api_check ():
    output=command_runner("/home/ubuntu/hoopla/api-call.sh api2/json/cluster")
    crap=list(output.keys())[0]
    if crap == "data":
        logging.info("Can access the proxmox api using creds stored in the db\n")
        return True
    else:
        logging.info("Failed to acceess proxmox api using creds stored in the db\n")
        return False

def command_runner (command):
    command_arr = command.split(" ")
    logging.debug(command_arr)
    result=subprocess.run(command_arr, capture_output=True, text=True).stdout
    return json.loads(result)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    main()

#!/bin/bash

filecheck () {
	TEST_PASS="PASS"
	FILE=$HOME/hoopla-venv
	if ! test -d "$FILE"; then
		TEST_PASS="FAIL"
	fi
	FILE=$HOME/hoopla-venv/bin/python
	if ! test -f "$FILE"; then
		TEST_PASS="FAIL"
        fi
	FILE="/usr/local/bin/hoopla-venv"
	if [ ! -L ${FILE} ]; then
		TEST_PASS="FAIL"
	fi
	if [ ! -e ${FILE} ]; then
		TEST_PASS="FAIL"
	fi
	echo $TEST_PASS
}

python_code_test () {
	echo "#!/usr/local/bin/hoopla-venv" > /tmp/test.py
	echo "print('test')" >> /tmp/test.py
	chmod +x /tmp/test.py
	output=$(/tmp/test.py)
	rm -f /tmp/test.py
	if [ $output == "test" ]; then
		echo "PASS"
	else
		echo "FAIL"
	fi

}

main () {
	TEST_RESULT="FAIL"
	FILE_TEST_RESULT=$(filecheck)
	CODE_TEST_RESULT=$(python_code_test)
	if [ $FILE_TEST_RESULT == $CODE_TEST_RESULT ]; then
		if [ $FILE_TEST_RESULT == "PASS" ]; then
			TEST_RESULT="PASS"
		fi
	fi
	printf "\nPYTHON INSTALL TEST: $TEST_RESULT\n\n"
}
main
